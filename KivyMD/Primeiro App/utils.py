import re


def extract_data_with_regex(data):
    new_data = []
    table = re.findall('<table(.*)</table>', data)
    if not table:
        return None
    else:
        string = table[0].replace('&nbsp;', '')
        addr_cep = re.findall(r'<td width="\d{1,3}">(.*?)</td>', string)
        addr_cep = [i if not i.startswith(
            '<a href=\"javascript:detalhaCep') else re.findall(r';">(.*?)<br><br>', i)[0] for i in addr_cep]
        city_sta = re.findall(r'<td>(.*?)</td>', string)
        if len(addr_cep) == len(city_sta):
            for i in range(0, len(addr_cep), 2):
                addr, cep, city, sta = addr_cep[i], addr_cep[i+1], city_sta[i], city_sta[i+1]
                    
                new_data.append([addr, cep, city, sta])
        return new_data